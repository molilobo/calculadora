''' ---------------------------
File: calc.py
Author: Raul Molina
Date: 06/02/24
Goal: this program shows the basics about python '''


from sys import argv

def suma (x,y):
    return x+y;
def resta (x,y):
    return x-y;
if __name__ == '__main__':
    print('1 mas 2 es : ',suma(1,2))
    print('3 mas 4 es : ',suma (3,4))
    print('6 menos 5 es : ',resta (6,5))
    print('8 menos 7 es : ', resta(8, 7))

'''
    practica con argv
    argv = 1 2 3 4 5 6 7 8
    print('1 mas 2 es : ',suma(argv[1],argv[2]))
    print('3 mas 4 es : ',suma (argv[3],argv[4]))
    print('6 menos 5 es : ',resta (argv[6],argv[5]))
    print('8 menos 7 es : ', resta(argv[8],argv[7])'''